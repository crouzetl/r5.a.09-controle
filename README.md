# r5.a.09-controle

## Lancement

### Etape 1

Installer loki grâce à la commande :

```sh
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
```

### Etape 2

Modifier le fichier host pour ajouter les lignes suivantes :

```
127.0.0.1 minio.controle
127.0.0.1 magento.controle
127.0.0.1 ghost.controle
127.0.0.1 grafana.controle
127.0.0.1 traefik.controle
127.0.0.1 odoo.controle
```

### Etape 3

Exécuter le script `init.sh` qui va lancer les différents docker-compose.

(erreur au lancement de l'init alors que tout marchait avant ?? j'espère que ça marchera chez vous)
(il faut surement lancer plusieurs fois l'init en partant de rien pour que ça marche)
