#!/bin/bash

# lancer les conteneurs
docker-compose -f loki/docker-compose.yml up -d
pause 5
docker-compose -f traefik/docker-compose.yml up -d
pause 5
docker-compose -f ghost/docker-compose.yml up -d
pause 5
docker-compose -f minio/docker-compose.yml up -d
pause 5
docker-compose -f magento/docker-compose.yml up -d
pause 5
docker-compose -f odoo/docker-compose.yml up -d

